C.F.G.S. Desarrollo de Aplicaciones Web# retratosrobot
Módulo de Programación - Curso 2020/2021
Autor: Marian García Sánchez

Programa para crear retratos robot con cadenas de caracteres.
Una vez ejecutado el programa, el usuario puede crear tantos retratros como desee antes de finalizar.
Los retratos se crean eligiendo los rasgos de cada facción con las opciones siguientes:

    PELO                          OJOS                  NARIZ                BOCA                BARBILLA
    1. WWWWWWWWW            1. | 0   0 |            1. @   J   @         1. |  ===  |          1. \_______/
    2. \\\//////            2. |-(. .)-|            2. {   "   }         2. |   -   |          2. \,,,,,,,/ 
    3. |"""""""|            3. |-(o o)-|            3. [   j   ]         3. |  ___  |
    4. |||||||||            4. | \   / |            4. <   -   >         4. |  ---  |


Una vez elegidos los rasgos de cada facción, se mostrará el retrato por pantalla. 
Por Ejemplo, las opciones 3, 2, 1, 4 y 2 para pelo, ojos, nariz, boca y barbilla respectivamente,
muestran el siguiente retrato robot:

                                            |"""""""|
                                            |-(. .)-|
                                            @   J   @
                                            |  ---  |
                                            \,,,,,,,/